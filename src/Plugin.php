<?php
/**
 * @link      https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license   MIT
 */

namespace flowsa\flowguestentries;

use Craft;
use craft\elements\User;
use flowsa\flowguestentries\models\Settings;
use craft\models\Section;

/**
 * Class Plugin
 *
 * @property Settings $settings
 * @method Settings getSettings()
 */
class Plugin extends \craft\base\Plugin
{
    // Properties
    // =========================================================================

    /**
     * @inheritdoc
     */
    public $schemaVersion = '2.0.0';

    /**
     * @inheritdoc
     */
    public $hasCpSettings = false;

   
}
